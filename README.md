# mpfb

Docker image testbed for building CLI (legacy) Fortran applications on multiple platforms

The intial goal is to support GCC9, CMake, and platform-specific packagers for Debian/Ubuntu, Fedora, and (if possible) OSX and Windows.
